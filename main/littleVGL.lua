---@diagnostic disable: lowercase-global, undefined-global
module(...,package.seeall)

require "ILI9806E" --加载LCD
require "msg2238" --加载tp
-- require "camera"
require "lvsym"
require "ntp"
require "misc"
require "scanCode"
require "audio"
require "common"
require "pmd"

require "iCoolStyle"
require "iCoolIdle"
--require "iCoolMain_Widgets"
require "iCoolMulti_Widgets"
--require "iCoolSingle_Widgets"
require "iCoolClock"
require "iCoolFloder"
require "iCoolAudio"
require "iCoolCalendar"
require "iCoolQrCode"
require "iCoolCalculator"
--require "iCoolWeather"
require "iCoolBlueTooth"


--全局界面储存表  
--此表存储的是iCool手机中各个界面的基容器信息  
--(禁止随意改动)  
--[[
--界面序列表查询  
{  
	界面         	序列  
	Idle          	 1  
	blueTooth			 2  
	Widgets          3  
	Clock			 4  
	Weather          5  
	Calculator		 6  
	Floder         	 7  
	Audio			 8  
	Calendar 		 9  
	QrCode    		 10  
}
]]
_G.idleScreenIndex = 1  
_G.blueToothScreenIndex = 2  
_G.widgetsScreenIndex = 3  
_G.widgets_MultiScreenIndex = 31  
_G.widgets_SingleScreenIndex = 32  
_G.clockScreenIndex = 4  
_G.weatherScreenIndex = 5  
_G.calculatorScreenIndex = 6  
_G.floderScreenIndex = 7  
_G.audioScreenIndex = 8  
_G.calendarScreenIndex = 9  
_G.qrCodeScreenIndex = 10  

_G.iCoolScreenTable = {}
for i = 1, 10 do
    iCoolScreenTable[i] = {}
    for j = 1, 2 do
        iCoolScreenTable[i][j] = 0
    end
end
--当前界面数量
curICoolScreenNumber = 0

local data = {type = lvgl.INDEV_TYPE_POINTER}
local function input()
	pmd.sleep(100)
	local ret,ispress,px,py = msg2238.get();
	if ret then
		if lastispress == ispress and lastpx == px and lastpy == py then
			return data
		end
		lastispress = ispress
		lastpx = px
		lastpy = py
		if ispress then
			tpstate = lvgl.INDEV_STATE_PR
		else
			tpstate = lvgl.INDEV_STATE_REL
		end
	else
		return data;
	end

	local topoint = {x = px,y = py}
	data.state = tpstate
	data.point = topoint

	return data
end

local function playSuccess(item,result)
	log.info("Play Success", item, result)
end

local function iCoBtnHandle(obj, e)
	local isPlay = false
	if (e == lvgl.EVENT_CLICKED)then
		if (isPlay)then
			--audio.stop()
			log.info("[MP3 is Stop]")
			isPlay = false
		else
			audio.setVolume(7)
			audio.play(3, "FILE", "/lua/mood.mp3", audiocore.VOL7, nil, true)
			log.info("[MP3 is Playing]")
			isPlay = true
		end
	end
end

function iCoolInit(id, ispress)
	--主界面初始化(演示其他Demo时必须添加此函数)
	Idle_Init()

	--main_WidgetsInit()
    --Multi_WidgetsInit()
    --Single_WidgetsInit()
    --iCoolTimeInit()
    --floderInit()
    --AudioInit()
    --CalendarInit()
	--QrCodeInit()

end

local function init()
	lvgl.init(iCoolInit, input)
	pmd.ldoset(15,pmd.PLATFORM_LDO_VIBR)
end

sys.taskInit(init, nil)

rtos.sleep(4000)

--界面自动注册函数  
--@obj  	要注册的界面基容器  
--@index	要注册的界面的序列号  
function _G.iCoolScreenRegister(beRegisterScreen, beRegisterScreenIndex)
	--统计当前已注册的界面数量
	local curBeRegisterScreenNumber = 0
	for i = 1, 10 do
		if (_G.iCoolScreenTable[i][1] ~= 0)then
			curBeRegisterScreenNumber = curBeRegisterScreenNumber + 1
		end
	end
	log.info(string.format("[iCoolScreenRegister]已注册的界面数为:%d", curBeRegisterScreenNumber))

	--进行界面的注册
	local curBeRegisterAfterScreenNumber = curBeRegisterScreenNumber + 1
	_G.iCoolScreenTable[curBeRegisterAfterScreenNumber][1] = beRegisterScreen
	_G.iCoolScreenTable[curBeRegisterAfterScreenNumber][2] = beRegisterScreenIndex
	if ((_G.iCoolScreenTable[curBeRegisterAfterScreenNumber][1] ~= 0) and (_G.iCoolScreenTable[curBeRegisterAfterScreenNumber][2] ~= 0) )then
		log.info(string.format("[iCoolScreenRegister]当前注册的界面数为:%d", curBeRegisterAfterScreenNumber))
		for i=1, curBeRegisterAfterScreenNumber do
			print(string.format("第%d个界面参数如下:", i))
			for j=1, 2 do
				if (j == 1)then
					print("界面为:")
					print(iCoolScreenTable[i][j])
				else
					print("界面index为:")
					print(iCoolScreenTable[i][j])
				end
			end
		end
	end
end

--iCool手机触摸按键控制函数
function iCoolTouchControl(id, ispress)
	log.info("菜单(id=1) home(id=4) 返回键(id=2)")
	if (id == 2)then
		--统计返回前的界面数量
		curICoolScreenNumber = 0
		for i = 1, 10 do
			if (_G.iCoolScreenTable[i][1] ~= 0)then
				curICoolScreenNumber = curICoolScreenNumber + 1
			end
		end
		log.info(string.format("[iCoolTouchControl]返回前界面数量为:%d", curICoolScreenNumber))

		--判断界面数量是否多余1个，只有多余一个才执行以下操作
		--执行返回后的措施
		if (curICoolScreenNumber > 1)then
			--关于Audio界面的返回附加操作
			if (_G.iCoolScreenTable[curICoolScreenNumber][2] == _G.audioScreenIndex)then
				if (_G.audio_InMusicPage)then
					lvgl.obj_del(_G.audio_MusicControl)
				elseif (_G.audio_InRecordPage) then
					lvgl.obj_del(_G.audio_RecordBtn)
				elseif (_G.audio_InVoicePage) then
					lvgl.obj_del(_G.audio_VoiceBtn)
				end
				_G.audio_ExistMusicControl = false
				_G.audio_ExistRecordBtn = false
				_G.audio_ExistVoiceBtn = false
			--关于Clock界面的返回附加操作
			elseif (_G.iCoolScreenTable[curICoolScreenNumber][2] == _G.clockScreenIndex) then
				sys.timerStop(_G.clock_StopWatchDisplayHandle, nil)
				sys.timerStop(_G.getTimeOneSec, "getTime")
			--多控件界面的返回附加操作
			elseif (_G.iCoolScreenTable[curICoolScreenNumber][2] == widgets_MultiScreenIndex) then
				_G.Multi_DeleteAllAnim()
			--蓝牙界面的返回附加操作
			elseif (_G.iCoolScreenTable[curICoolScreenNumber][2] == blueToothScreenIndex) then
				btcore.scan(0)
			end
			lvgl.obj_del(_G.iCoolScreenTable[curICoolScreenNumber][1])
			_G.iCoolScreenTable[curICoolScreenNumber][1] = 0
			_G.iCoolScreenTable[curICoolScreenNumber][2] = 0
		end
		--统计返回后的界面数量
		curICoolScreenNumber = 0
		for j = 1, 10 do
			if (_G.iCoolScreenTable[j][1] ~= 0)then
				curICoolScreenNumber = curICoolScreenNumber + 1
			end
		end
		log.info(string.format("[iCoolTouchControl]返回后界面数量为:%d", curICoolScreenNumber))
	end
end

function iCoolClick(id, ispress)
	if (id == 2)then
		lvgl.obj_del(FLODER_BASECONT)
		Idle_AppInit()
	end
end

msg2238.cb(iCoolTouchControl)

-- 声音键 -
local function volkey(msg)
    log.info("音乐键 下", msg)
end
pins.setup(10,volkey,pio.PULLUP)

--开机键
local function keyMsg(msg)
    log.info("开机键",msg.key_matrix_row,msg.key_matrix_col,msg.pressed)
	rtos.restart()
end
rtos.closeSoftDog()
rtos.on(rtos.MSG_KEYPAD,keyMsg)
rtos.init_module(rtos.MOD_KEYPAD,0,0,0)
