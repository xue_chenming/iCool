---@diagnostic disable: lowercase-global, undefined-global

--完全透明样式:背景和边框透明
absTransStyle = lvgl.style_t()
lvgl.style_init(absTransStyle)
lvgl.style_set_bg_opa(absTransStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)
lvgl.style_set_border_opa(absTransStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)

--部分透明样式:边框透明
borderTransStyle = lvgl.style_t()
lvgl.style_init(borderTransStyle)
lvgl.style_set_border_opa(borderTransStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)

--字体默认颜色样式
defaultFontStyle = lvgl.style_t()
lvgl.style_init(defaultFontStyle)
lvgl.style_set_text_color(defaultFontStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0x1C1C1C))

--被选中的字体颜色样式
selectedFontStyle = lvgl.style_t()
lvgl.style_init(selectedFontStyle)
lvgl.style_set_text_color(selectedFontStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0x1E90FF))

--Audio界面总样式
audio_PageStyle = lvgl.style_t()
lvgl.style_init(audio_PageStyle)
lvgl.style_set_bg_color(audio_PageStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))
lvgl.style_set_border_opa(audio_PageStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)

--Audio:Music界面控制栏颜色样式
audio_MusicControlStyle = lvgl.style_t()
lvgl.style_init(audio_MusicControlStyle)
lvgl.style_set_bg_color(audio_MusicControlStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xBEBEBE))
lvgl.style_set_border_opa(audio_MusicControlStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)

--Audio:Music界面控制栏按钮样式
audio_MusicControlBtnStyle = lvgl.style_t()
lvgl.style_init(audio_MusicControlBtnStyle)
lvgl.style_set_bg_color(audio_MusicControlBtnStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xB1B1B1))

--Audio:Record界面录音按钮样式
Audio_RecordBtnStyle = lvgl.style_t()
lvgl.style_init(Audio_RecordBtnStyle)
lvgl.style_set_bg_color(Audio_RecordBtnStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xDC143C))

--Audio:Record界面录音按钮默认样式
Audio_RecordBtnDefaultStyle = lvgl.style_t()
lvgl.style_init(Audio_RecordBtnDefaultStyle)
lvgl.style_set_bg_color(Audio_RecordBtnDefaultStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))

--QrCode界面总样式
qrcode_PageStyle = lvgl.style_t()
lvgl.style_init(qrcode_PageStyle)
lvgl.style_set_bg_color(qrcode_PageStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0x696969))
lvgl.style_set_border_opa(qrcode_PageStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)

--QrCode界面字体默认样式
qrcode_FontStyle = lvgl.style_t()
lvgl.style_init(qrcode_FontStyle)
lvgl.style_set_text_color(qrcode_FontStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))

--QrCode:CreateQrCode界面显示二维码区域样式
qrcode_DisplayAreaStyle = lvgl.style_t()
lvgl.style_init(qrcode_DisplayAreaStyle)
lvgl.style_set_bg_color(qrcode_DisplayAreaStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0x778899))
lvgl.style_set_border_opa(qrcode_DisplayAreaStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)

--Widgets界面总样式
widgets_PageStyle = lvgl.style_t()
lvgl.style_init(widgets_PageStyle)
lvgl.style_set_bg_color(widgets_PageStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0x33CAFF))
lvgl.style_set_border_opa(widgets_PageStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)

--Calculator界面: 显示区域样式
calcu_DisplayResultAreaStyle = lvgl.style_t()
lvgl.style_init(calcu_DisplayResultAreaStyle)
lvgl.style_set_bg_color(calcu_DisplayResultAreaStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0x4E5C65))
lvgl.style_set_border_opa(calcu_DisplayResultAreaStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)
lvgl.style_set_text_color(calcu_DisplayResultAreaStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xF8F8FF))

--Calculator界面: 按钮区域样式
calcu_BtnAreaStyle = lvgl.style_t()
lvgl.style_init(calcu_BtnAreaStyle)
lvgl.style_set_bg_color(calcu_BtnAreaStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0x3C434B))
lvgl.style_set_border_opa(calcu_BtnAreaStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)

--Calculator界面: 按钮样式
calcu_BtnStyle = lvgl.style_t()
lvgl.style_init(calcu_BtnStyle)
lvgl.style_set_radius(calcu_BtnStyle, lvgl.STATE_DEFAULT, 0)
lvgl.style_set_bg_color(calcu_BtnStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0x3C434B))
lvgl.style_set_bg_color(calcu_BtnStyle, (lvgl.STATE_PRESSED or lvgl.STATE_CHECKED), lvgl.color_hex(0x3C434B))
lvgl.style_set_border_color(calcu_BtnStyle, (lvgl.STATE_PRESSED), lvgl.color_hex(0xF8F8FF))
lvgl.style_set_border_opa(calcu_BtnStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)
lvgl.style_set_text_color(calcu_BtnStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xF8F8FF))

--BlueTooth界面总样式
blueTooth_PageStyle = lvgl.style_t()
lvgl.style_init(blueTooth_PageStyle)
lvgl.style_set_bg_color(blueTooth_PageStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0x3C434B))
lvgl.style_set_border_opa(blueTooth_PageStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)

--蓝牙扫描按钮样式
blueTooth_ScanBtnStyle = lvgl.style_t()
lvgl.style_init(blueTooth_ScanBtnStyle)
lvgl.style_set_bg_color(blueTooth_ScanBtnStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0x8A2BE2))
lvgl.style_set_bg_color(blueTooth_ScanBtnStyle, (lvgl.STATE_PRESSED or lvgl.STATE_CHECKED), lvgl.color_hex(0x8A2BE2))
lvgl.style_set_border_opa(blueTooth_ScanBtnStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)

--BlueTooth界面字体样式
blueTooth_FontStyle = lvgl.style_t()
lvgl.style_init(blueTooth_FontStyle)
lvgl.style_set_text_color(blueTooth_FontStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))

