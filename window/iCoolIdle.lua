---@diagnostic disable: undefined-global, lowercase-global

mainPage_Style = lvgl.style_t()
mainPage_BarStyle = lvgl.style_t()
mainPage_FontStyle = lvgl.style_t()
mainPage_IconStyle = lvgl.style_t()
--主界面容器
SCREEN_MAIN = nil
--主界面
mainPageTableView = nil
--主界面的第一个页面
main_FirstPage = nil
--主界面的第二个页面
main_SecondPage = nil
--界面表
pagesTable = {}
--状态栏
mainPage_Bar = nil
--状态栏:时间
mainPage_Bar_Clock = nil
--apps_Widgets
mainPage_appBtn_widgets = nil

local function appHandle_Setting(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Setting")
    end
end

local function appHandle_BlueTooth(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]BlueTooth")
        iCoolBlueToothInit()
    end
end

local function appHandle_Audio(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Audio")
        AudioInit()
    end
end

local function appHandle_QRcode(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]QRcode")
        QrCodeInit()
    end
end

local function appHandle_Calendar(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Calendar")
        CalendarInit()
    end
end

local function appHandle_Store(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Store")
    end
end

local function appHandle_Floder(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Floder")
        floderInit()
    end
end

local function appHandle_Calculator(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Calculator")
        iCoolCalculatorInit()
    end
end

local function appHandle_Weather(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Weather")
    end
end

local function appHandle_Clock(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Clock")
        iCoolTimeInit()
    end
end

local function appHandle_Camera(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Camera")
    end
end

local function appHandle_Widgets(btn, e)
    if (e == lvgl.EVENT_CLICKED)then
        log.info("[APP Start]Widgets")
        Multi_WidgetsInit()
    end
end


--apps当前数量
appsNumber = 0

--apps信息储存表  
--@string 第一个参数是app的显示名称  
--@number 第二个参数是app的显示长度(默认为100)  
--@number 第三个参数是app的显示宽度(默认为100)  
--@string 第四个参数是app的图标地址(格式为 "/lua/图标名称的bin文件" 也可以是png和jpg格式)  
--@number 第五个参数是app在界面上的位置(相对于TabView中页面的水平距离)  
--@number 第六个参数是app在界面上的位置(相对于TabView中页面的垂直距离)  
--@nil 第七个参数保存的是app的image对象  
--@nil 第八个参数保存的是app名称的Label对象  
--@nil 第九个参数保存的是app事件响应的button对象  
--@function 第十个参数是app事件响应的响应函数  
-----------------------------------------------------
--提示：若要增加自定义参数，可添加在表的后续位置
--app之间横坐标为:16/132/248/364    (相隔116)
--app之间纵坐标为:30/200/370        (相隔170)
appsTable =
{
    {"控件", 100, 100, "/lua/Widgets.png", 16, 30, nil, nil, nil, appHandle_Widgets},
    {"时钟", 100, 100, "/lua/Clock.png", 132, 30, nil, nil, nil, appHandle_Clock},
    {"天气", 100, 100, "/lua/Weather.png", 248, 30, nil, nil, nil, appHandle_Weather},
    {"计算器", 100, 100, "/lua/Calculator.png", 364, 30, nil, nil, nil, appHandle_Calculator},
    {"文件", 100, 100, "/lua/Floder.png", 16, 200, nil, nil, nil, appHandle_Floder},
    {"音频", 100, 100, "/lua/Audio.png", 132, 200, nil, nil, nil, appHandle_Audio},
    {"日历", 100, 100, "/lua/Calendar.png", 248, 200, nil, nil, nil, appHandle_Calendar},
    {"二维码", 100, 100, "/lua/QrCode.png", 364, 200, nil, nil, nil, appHandle_QRcode},
    {"蓝牙", 100, 100, "/lua/BlueTooth.png", 16, 370, nil, nil, nil, appHandle_BlueTooth}
    --{"照相", 100, 100, "/lua/Camera.bin", 16, 30, nil, nil, nil, appHandle_Camera},
    --{"商店", 100, 100, "/lua/Store.bin", 248, 200, nil, nil, nil, appHandle_Store},
    --{"Setting", 100, 100, "/lua/Setting.bin", 364, 370, nil, nil, nil, appHandle_Setting},
}

--apps自动生成函数
local function appsAutoProduce()
    appsNumber = appsNumber + 1
    curPage = pagesTable[1]

    --主界面app显示图标
    mainPage_app = lvgl.img_create(curPage, nil)
    appsTable[appsNumber][7] = mainPage_app
    lvgl.img_set_src(appsTable[appsNumber][7], appsTable[appsNumber][4])
    lvgl.obj_set_size(appsTable[appsNumber][7], appsTable[appsNumber][2], appsTable[appsNumber][3])
    lvgl.obj_align(appsTable[appsNumber][7], curPage, lvgl.ALIGN_IN_TOP_LEFT, appsTable[appsNumber][5], appsTable[appsNumber][6])
    lvgl.obj_add_style(appsTable[appsNumber][7], lvgl.IMG_PART_MAIN, mainPage_Style)

    --主界面app的名称
	mainPage_appLabel = lvgl.label_create(curPage, nil)
    appsTable[appsNumber][8] = mainPage_appLabel
	lvgl.obj_align(appsTable[appsNumber][8], appsTable[appsNumber][7], lvgl.ALIGN_CENTER, 0, 62)
	lvgl.label_set_text(appsTable[appsNumber][8], appsTable[appsNumber][1])

    --主界面app的响应事件按钮
    mainPage_appBtn = lvgl.btn_create(appsTable[appsNumber][7], nil)
    appsTable[appsNumber][9] = mainPage_appBtn
    lvgl.obj_set_size(appsTable[appsNumber][9], appsTable[appsNumber][2], appsTable[appsNumber][3])
    lvgl.obj_align(appsTable[appsNumber][9], curPage, lvgl.ALIGN_IN_TOP_LEFT, appsTable[appsNumber][5], appsTable[appsNumber][6])
    lvgl.obj_add_style(appsTable[appsNumber][9], lvgl.BTN_PART_MAIN, mainPage_Style)
    lvgl.obj_set_event_cb(appsTable[appsNumber][9], appsTable[appsNumber][10])
end

--加载app
function Idle_AppInit()
    --主界面
    mainPageTableView = lvgl.tabview_create(SCREEN_MAIN, nil)
    lvgl.obj_set_size(mainPageTableView, 480, 804)
    lvgl.obj_align(mainPageTableView, nil, lvgl.ALIGN_IN_TOP_MID, 0, 50)
    lvgl.obj_add_style(mainPageTableView, lvgl.TABVIEW_PART_BG, mainPage_Style)
    lvgl.obj_add_style(mainPageTableView, lvgl.TABVIEW_PART_TAB_BG, mainPage_Style)
    
    --主界面添加俩个滑动屏幕
    main_FirstPage = lvgl.tabview_add_tab(mainPageTableView, "")
    main_SecondPage = lvgl.tabview_add_tab(mainPageTableView, "")

    lvgl.obj_add_style(main_FirstPage, lvgl.PAGE_PART_SCROLLBAR, mainPage_BarStyle)
    lvgl.obj_add_style(main_SecondPage, lvgl.PAGE_PART_SCROLLBAR, mainPage_BarStyle)

    pagesTable[1] = main_FirstPage
    pagesTable[2] = main_SecondPage

    --遍历循环自动生成app
    for k, v in pairs(appsTable) do
        appsAutoProduce()
        lvgl.obj_add_style(appsTable[appsNumber][7], lvgl.IMG_PART_MAIN, mainPage_Style)
        lvgl.obj_add_style(appsTable[appsNumber][8], lvgl.LABEL_PART_MAIN, mainPage_FontStyle)
    end
end
--[[
local function mainPage_GetClock(Tag)
    main_GetClock = misc.getClock()
    lvgl.label_set_text(mainPage_Bar_Clock, string.format("%02d:%02d", main_GetClock.hour, main_GetClock.min))
end
]]
function Idle_Init()
    --主界面透明
    lvgl.style_init(mainPage_Style)
    lvgl.style_set_bg_opa(mainPage_Style, lvgl.STATE_DEFAULT, lvgl.OPA_0)
    lvgl.style_set_border_opa(mainPage_Style, lvgl.STATE_DEFAULT, lvgl.OPA_0)

    --界面状态栏样式
    lvgl.style_init(mainPage_BarStyle)
    lvgl.style_set_bg_color(mainPage_BarStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xDCDCDC))
    lvgl.style_set_bg_opa(mainPage_BarStyle, lvgl.STATE_DEFAULT, 2)
    lvgl.style_set_border_opa(mainPage_BarStyle, lvgl.STATE_DEFAULT, lvgl.OPA_0)

    --界面字体样式(暂时不支持改变字体样式)
    lvgl.style_init(mainPage_FontStyle)
	lvgl.style_set_text_color(mainPage_FontStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xFFFFFF))

    --手机app图标样式
    lvgl.style_init(mainPage_IconStyle)
    lvgl.style_set_bg_color(mainPage_IconStyle, lvgl.STATE_DEFAULT, lvgl.color_hex(0xDCDCDC))
    lvgl.style_set_radius(mainPage_IconStyle, lvgl.STATE_DEFAULT, lvgl.dpx(30))

    --总界面容器
    SCREEN_MAIN = lvgl.cont_create(lvgl.scr_act(), nil)
    lvgl.obj_set_size(SCREEN_MAIN, 480, 854)
    lvgl.obj_align(SCREEN_MAIN, nil, lvgl.ALIGN_CENTER, 0, 0)
    lvgl.obj_add_style(SCREEN_MAIN, lvgl.CONT_PART_MAIN, mainPage_BarStyle)

    --界面的注册以及记录
    _G.iCoolScreenRegister(SCREEN_MAIN, _G.idleScreenIndex)

    --手机背景
    mainBackGround = lvgl.img_create(SCREEN_MAIN, nil)
    lvgl.img_set_src(mainBackGround, "/lua/wallpaper03.jpg")
    lvgl.obj_set_size(mainBackGround, 480, 854)
    lvgl.obj_align(mainBackGround, nil, lvgl.ALIGN_CENTER, 0, 0)

    --总界面状态栏
    mainPage_Bar = lvgl.cont_create(mainBackGround, nil)
    lvgl.obj_set_width(mainPage_Bar, 480)
    lvgl.obj_set_height(mainPage_Bar, 50)
	lvgl.obj_align(mainPage_Bar, nil, lvgl.ALIGN_IN_TOP_MID, 0, 0)
    lvgl.obj_add_style(mainPage_Bar, lvgl.CONT_PART_MAIN, mainPage_BarStyle)

    --状态:时间
	mainPage_Bar_Clock = lvgl.label_create(mainPage_Bar, nil)
    lvgl.label_set_text(mainPage_Bar_Clock, "时间")
	lvgl.obj_align(mainPage_Bar_Clock, mainPage_Bar, lvgl.ALIGN_IN_LEFT_MID, 10, 0)
    lvgl.obj_add_style(mainPage_Bar_Clock, lvgl.LABEL_PART_MAIN, mainPage_FontStyle)
--[[
    --状态:信号/电量/蓝牙
    mainPage_SignalLabel = lvgl.label_create(mainPage_Bar, mainPage_Bar_Clock)
	lvgl.obj_align(mainPage_SignalLabel, mainPage_Bar, lvgl.ALIGN_IN_RIGHT_MID, -60, 0)
    lvgl.label_set_text(mainPage_SignalLabel, lvgl.SYMBOL_WIFI)

    mainPage_BlueToothLabel = lvgl.label_create(mainPage_Bar, mainPage_Bar_Clock)
	lvgl.obj_align(mainPage_BlueToothLabel, mainPage_Bar, lvgl.ALIGN_IN_RIGHT_MID, -30, 0)
    lvgl.label_set_text(mainPage_BlueToothLabel, lvgl.SYMBOL_BLUETOOTH)

    mainPage_BatteryLabel = lvgl.label_create(mainPage_Bar, mainPage_Bar_Clock)
	lvgl.obj_align(mainPage_BatteryLabel, mainPage_Bar, lvgl.ALIGN_IN_RIGHT_MID, 0, 0)
    lvgl.label_set_text(mainPage_BatteryLabel, lvgl.SYMBOL_BATTERY_3)
]]
    --加载主界面的内容
    Idle_AppInit()
end

--sys.timerLoopStart(mainPage_GetClock, 1000, "MAIN")