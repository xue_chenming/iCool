[TOC]

# 音频界面介绍

## 1. 音频：总体结构
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
音频界面总体结构分为3个部分，分别为音乐界面，录音界面以及语音界面  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
总体结构是由一个主界面 `audio_MainPage` ,来进行界面的显示，不同的是处于不同的界面，所显示的内容有所不同，为了显示不同的界面切换，在底部设计了一个导航栏 `audio_NavBar` 。在导航栏中添加了三个界面按钮，分别为音乐界面按钮 `audio_MusicPageBtn` ，录音界面按钮 `audio_RecordPageBtn` ，以及语音界面按钮 `audio_VoicePageBtn` ；通过点击导航栏中的按钮即可切换不同界面，做到在一个界面显示不同的内容。由于是在一个界面显示不同的内容，所以为此设计了一个状态转换机，部分代码如下：

```lua
--判断处于哪个界面:默认处于Music界面
_G.audio_InMusicPage = false
_G.audio_InVoicePage = false
_G.audio_InRecordPage = false

--音乐界面状态转换逻辑
if (_G.audio_ExistRecordBtn)then
    lvgl.obj_del(_G.audio_RecordBtn)
    _G.audio_RecordBtn = nil
    _G.audio_ExistRecordBtn = false
end
if (_G.audio_ExistVoiceBtn)then
    lvgl.obj_del(_G.audio_VoiceBtn)
    _G.audio_VoiceBtn = nil
    _G.audio_ExistVoiceBtn = false
end
```

详情请见Gitee代码 `/Window/iCoolAudio.lua`

## 2. 音频：音乐界面
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
音乐界面主要分为三个部分，分别为音乐控制容器 `audio_MusicControl` ，音乐播放详情 `audio_MusicPlayInfoLabel` 以及音乐显示列表 `audio_MusicList` ；  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
1.音乐控制容器也就是一个 `cont` 控件，在其中添加了三个控制按钮，分别为播放按钮 `audio_MusicPlaySongBtn` ，下一首按钮 `audio_MusciNextSongBtn` ，以及相对应的上一首按钮 `audio_MusicPreSongBtn` ，通过点击按钮，可以进行歌曲的切换和播放；  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
2.音乐播放详情也同样的是布置在了与音乐控制容器平齐的地方，当播放音乐时，它会相应的显示播放的音乐信息；  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
3.音乐显示列表显示的是手机所包含的所有的音频文件，它是一个 `list` 控件，通过遍历手机中的文件来显示到列表当中，可以通过点击相应的音乐来进行播放

```lua
audio_MusicPlaySongBtn = lvgl.btn_create(_G.audio_MusicControl, nil)
audio_MusciNextSongBtn = lvgl.btn_create(_G.audio_MusicControl, audio_MusicPlaySongBtn)
audio_MusicPreSongBtn = lvgl.btn_create(_G.audio_MusicControl, audio_MusciNextSongBtn)
audio_MusicPlayInfoLabel = lvgl.label_create(_G.audio_MusicControl, nil)
audio_MusicList = lvgl.list_create(audio_MainPage, nil)
```

## 3. 音频：录音界面
由于目前不支持麦克风功能，后续会及时更新

## 4. 音频：语音界面
由于目前不支持麦克风功能，后续会及时更新

计划效果展示：  
>   
![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210701143226942_2.png)  

目前效果展示：  
>   
![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210701142521592_audio.gif)  