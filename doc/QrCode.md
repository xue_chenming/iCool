[TOC]

# 二维码界面介绍

## 1. 二维码界面总体结构
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
二维码界面总体结构分为2个部分，分为二维码生成界面以及扫描二维码界面  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
总体结构主要是通过2个 `page` 来进行显示，分别为二维码生成界面 `qrcode_CreateQrCodePage` 和扫描二维码界面 `qrcode_ScanQrCodePage` ；这2个界面主要是通过2个按钮 `qrcode_CreateQrCodePageBtn` 以及 `qrcode_ScanQrCodePageBtn` 来进行界面的切换

```lua
qrcode_CreateQrCodePageBtn = lvgl.btn_create(qrcode_NavBar, nil)
lvgl.obj_set_event_cb(qrcode_CreateQrCodePageBtn, qrcode_JumpToCreateQrCodePageVar)

qrcode_ScanQrCodePageBtn = lvgl.btn_create(qrcode_NavBar, nil)
lvgl.obj_set_event_cb(qrcode_ScanQrCodePageBtn, qrcode_JumpToScanQrCodePageVar)
```

## 2. 二维码生成界面
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
二维码生成界面主要分为3个部分，分别为二维码显示部分 `qrcode_DisQrCodeArea` , 二维码内容输入部分 `qrcode_InputQrCodeDetail` 以及生成二维码按钮 `qrcode_CreateQrCodeBtn` ;  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
1. 二维码显示部分主要是通过创建一个显示容器 `cont` 来承载二维码的显示

```lua
qrcode_DisQrCodeArea = lvgl.cont_create(qrcode_CreateQrCodePage, nil)
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
2. 二维码内容输入区域是由控件 `textarea` 来进行内容的输入和获取，可以通过输入的内容来随之生成二维码

```lua
qrcode_InputQrCodeDetail = lvgl.textarea_create(qrcode_CreateQrCodePage, nil)
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
3. 二维码生成按钮顾名思义，也就是一个 `qrcode_CreateQrCodeBtn` 按钮控件，通过点击按钮就可以刷新 `qrcode_DisQrCodeArea` 中的内容，然后生成相对应的包含输入内容的二维码

## 3. 扫描二维码界面
> 由于目前暂不支持拍摄功能，所以没有添加此界面功能，后续也会及时更新


计划效果：
>   
![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210701143222885_1.png)

目前效果展示：  
>  
![](http://openluat-luatcommunity.oss-cn-hangzhou.aliyuncs.com/images/20210701142836896_qrcode.gif)

